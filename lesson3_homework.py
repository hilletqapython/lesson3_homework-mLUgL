"""
Task 1
Write a cycle that will require the user to enter a word in which there is a letter "o"
(both large and small are taken into account). The loop should not end if the user enters
a word without the letter "o".
"""

# Task 1

condition = True
while condition:
    user_input = input("Enter a word with 'o' in it: ").lower()
    check_for_mistakes = user_input.isalpha()   # in case you accidentally put digits or spaces inside your word
    if check_for_mistakes:
        if "o" in user_input:
            print("You got letter 'o' in your word.")
            condition = False
        else:
            print("There is no letter 'o' in your word. Please re-enter a word")
    else:
        print("Incorrect input. Please re-enter a word")  # if there is digit in your word

print()


"""
Task 2
There is a list with data lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
Write code that creates a new list (such as lst2) that contains only string variables that are present in lst1. 
Note that lst1 is not static and can be formed dynamically from start to start.
"""

# Task 2

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = []
for element in lst1:
    if type(element) == str:
        lst2.append(element)

print(lst1)
print(lst2)

print()


"""
Task 3
There is a string with a specific text (you can use input or a constant). 
Write code that will determine the number of words in this text that end 
in "o" (both large and small are taken into account).
"""

# Task 3

result = 0
user_input = input('Enter a text: ').lower()    # Example text: Do you got an info about this company logo
list_of_words = user_input.split(' ')
for word in list_of_words:
    if word[-1] == 'o':
        result += 1

if result == 1:
    print(f'You got {result} word that end with "o"')
else:
    print(f'You got {result} words that end with "o"')









